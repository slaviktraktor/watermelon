# Watermelon

Watermelon is a service that allows you to download and upload any images you want. Images are not protected by copyright, so everyone can use it everywhere.

# Setting up #
## Create ./api/.env file contains:

LISTEN_PORT=*listen port, example 1234*

DB_PORT=*database port, example 12345*

DB_NAME=*database name, example watermelon*

## Create ./front/.env.development.local (or .env.produstion.local for production) file contains:

REACT_APP_SERVER_IP=*API url, example http://127.0.0.1:1234/*