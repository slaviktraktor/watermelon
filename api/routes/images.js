import express from 'express';
import fs from 'fs';
import path from 'path';
import * as db from '../utils/dbutils';

let router = express.Router();

router.get('/dblist', (req, res) => {
  db.dblist()
    .then(data => res.send(data))
    .catch(error => res.send(error));
});

router.post('/list', (req, res) => {
  if (req.body.tags === undefined || req.body.tags.length === 0) {
    db.nameslist(req.body.count, req.body.offset)
      .then(data => res.send(data))
      .catch(error => res.send(error));
  } else {
    const tmp = req.body.tags.join('~').toLowerCase();
    const lcTags = tmp.split('~');
    db.nameslisttags(req.body.count, req.body.offset, lcTags)
      .then(data => res.send(data))
      .catch(error => res.send(error));
  }
});

router.get('/img/:name', (req, res) => {
  db.FindByName(req.params.name).then(data => {
    res.sendFile(path.resolve(`${__dirname}/../savedimages/${req.params.name}`));
  });
});

router.get('/imginfo/:name', (req, res) => {
  db.FindByNameFull(req.params.name).then(data => {
    res.send(data);
  });
});

router.post('/add', (req, res) => {
  if (!req.body) return res.sendStatus(400);

  req.body.name = db.generate(30) + req.body.name;
  const tmpTags = req.body.tags.join('~').toLowerCase();
  const resTags = tmpTags.split('~');

  db.Add(
    req.body.name,
    req.body.type,
    resTags,
    req.body.desc,
    req.body.title
  );

  fs.writeFile(
    `${__dirname}/../savedimages/${req.body.name}`,
    req.body.code,
    'base64',
    err => {
      console.log(err);
    }
  );
  res.send('Object saved');
});

/*router.delete("/del/:id", (req, res) => {
    db.DelById(req.params.id).then(data => res.send(data)); 
});*/

export default router;
