import mongoose from 'mongoose';
import '../models/image';
const Image = mongoose.model('Image');

export function setUpConnection() {
  mongoose.Promise = global.Promise;
  mongoose
    .connect(
      `mongodb://localhost:${process.env.DB_PORT}/${process.env.DB_NAME}`,
      { useUnifiedTopology: true, useNewUrlParser: true }
    )
    .catch(e => console.log(e.name + ': ' + e.message));
  return mongoose;
}

export function dblist() {
  return Image.find(
    {},
    {
      _id: 0,
      __v: 0,
    }
  );
}

export function generate(len) {
  var text = '';
  var possible =
    'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (var i = 0; i < len; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  return text;
}

export function nameslist(icount, ioffset) {
  return Image.find(
    {},
    {
      _id: 0,
      name: 1,
    },
    { limit: icount, skip: icount * (ioffset - 1) }
  ).sort({ crdate: 'desc' });
}

export function nameslisttags(icount, ioffset, itags) {
  return Image.find(
    { tags: itags },
    {
      _id: 0,
      name: 1,
    },
    { limit: icount, skip: icount * (ioffset - 1) }
  ).sort({ crdate: 'desc' });
}

export function FindByName(ImgName) {
  return Image.find(
    { name: ImgName },
    {
      _id: 0,
      code: 1,
    }
  );
}

export function FindByNameFull(ImgName) {
  return Image.find(
    { name: ImgName },
    {
      _id: 0,
      __v: 0,
      code: 0,
    }
  );
}

export function Add(ImgName, ImgType, ImgTags, ImgDesc, ImgTitle) {
  var im = new Image({
    name: ImgName,
    type: ImgType,
    tags: ImgTags,
    desc: ImgDesc,
    title: ImgTitle,
    crdate: new Date(),
  });
  im.save(err => {
    if (err) return console.log(err);
  });
}

/*export function DelById(ImgId) {
    return Image.deleteOne({id:ImgId}, {
        _id: 0
    });
} 

export function UpdById(ImgId) {
    return Image.deleteOne({id:ImgId}, {
        _id: 0
    });
} */

/*router.get("/:id", (req, res) => {
    console.log(req.params.id)
 Image.find({id:req.params.id}).then(data => res.send(data)); 
});*/
