import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ImgScheme = new Schema({
    name: String,
    type: String,
    tags: Array,
    desc: String,
    title: String,
    crdate: Date
});

mongoose.model('Image', ImgScheme);

