﻿import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';
import dotenv from 'dotenv';
//import {MongoClient, ObjectId} from 'mongodb';
//import cookieParser from 'cookie-parser';

// ROUTES
import images from './routes/images';
import * as db from './utils/dbutils'

// load .env
dotenv.config();
const app = express();

db.setUpConnection();

// Using bodyParser middleware
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json({
    limit: '50mb',
}));


// Allow requests from any origin
app.use(cors({origin: '*'}));

app.use('/images', images);


const port = process.env.LISTEN_PORT;
app.listen(port, function(){
    console.log(`Сервер ожидает подключения на порту ${port}...`);
});
