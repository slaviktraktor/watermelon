import api from '../api/index';

export function addImageAction(file, name, type, title, desc, tags) {
  return dispatch => {
    api
      .createImage({
        name: name,
        type: type,
        code: file,
        tags: tags,
        title: title,
        desc: desc,
      })
      .then(() => {
        dispatch({
          type: 'ADD_IMAGE',
        });
      })
      .catch(err => {
        dispatch({
          type: 'ADD_IMAGE_ERROR',
        });
      });
  };
}

export function loadImagesAction(count, offset = 1, start = false) {
  return dispatch => {
    api
      .listImages({
        count: count,
        offset: offset,
      })
      .then(res => {
        let list = res.data.map(v => v.name);
        if (start)
          dispatch({
            type: 'GET_START_IMAGES',
            payload: list,
          });
        else
          dispatch({
            type: 'GET_ADDITIONAL_IMAGES',
            payload: list,
          });
      })
      .catch(err => {
        dispatch({
          type: 'GET_IMAGES_ERROR',
        });
      });
  };
}

export function openBigAction(img) {
  return dispatch => {
    dispatch({
      type: 'OPEN_BIG_IMAGE',
      payload: img,
    });
  };
}

export function loadImageMeta(img) {
  return dispatch => {
    api
      .getImageMeta(img)
      .then(res => {
        dispatch({
          type: 'IMAGE_META',
          payload: {
            name: res.data[0].name,
            tags: res.data[0].tags,
            desc: res.data[0].desc,
            title: res.data[0].title,
          },
        });
      })
      .catch();
  };
}

export function closeBigAction() {
  return dispatch => {
    dispatch({
      type: 'CLOSE_BIG_IMAGE',
    });
  };
}
