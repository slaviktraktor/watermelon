import { openBigAction, closeBigAction } from "./imagesActions";

it("openBigAction test", () => {
  const img = "kek";
  const type = "OPEN_BIG_IMAGE";
  const actionFunc = openBigAction("kek");

  actionFunc((action) => {
    expect(action.payload).toEqual(img);
    expect(action.type).toEqual(type);
  });
});

it("closeBigAction test", () => {
  const type = "CLOSE_BIG_IMAGE";
  const actionFunc = closeBigAction();

  actionFunc((action) => {
    expect(action.type).toEqual(type);
  });
})