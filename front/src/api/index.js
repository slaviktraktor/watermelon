import axios from "axios"

const apiPrefix = `${process.env.REACT_APP_SERVER_IP}`;

export default {
    listImages(data) {
        return axios.post(`${apiPrefix}images/list/`, data);
    },
    getImage(name) {
        return axios.get(`${apiPrefix}images/img/${name}`);
    },
    createImage(data) {
        return axios.post(`${apiPrefix}images/add`, data);
    },
    getImageMeta(img){
        return axios.get(`${apiPrefix}images/imginfo/${img}`);
    }
}