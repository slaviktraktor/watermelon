import React from "react";
import ReactDOM from "react-dom";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";

import { Root } from "./Root";
import rootReducer from "./reducers/index";

it("Root renders without crashing", () => {
  const store = createStore(rootReducer, applyMiddleware(thunk));
  const div = document.createElement("div");
  ReactDOM.render(<Root store={store} />, div);
  ReactDOM.unmountComponentAtNode(div);
});
