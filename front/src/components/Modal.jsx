import React, { Component } from 'react';
import ReactDOM from 'react-dom';

export default class Modal extends Component {
  componentWillMount() {
    this.root = document.createElement('div');
    document.body.appendChild(this.root);
  }

  componentWillUnmount() {
    document.body.removeChild(this.root);
  }

  render() {
    return ReactDOM.createPortal(
      <>
        <div
          className={this.props.className}
          onClick={this.props.onClose}
        ></div>
        {this.props.children}
      </>,
      this.root
    );
  }
}
