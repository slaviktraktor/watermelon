import React, { Component } from 'react';

import Header from './Header.jsx';
import Main from './Main.jsx';

import './App.scss';

class App extends Component {
  render() {
    return (
      <>
        <Header />
        <div className="wrapper">
          <Main />
        </div>
      </>
    );
  }
}

export default App;
