import React, { Component } from 'react';

import ImagesContainer from '../containers/ImagesContainer/ImagesContainer.jsx';

import './Feed.scss';

export default class Feed extends Component {
  render() {
    return (
      <>
        <nav>
          <input
            type="text"
            placeholder="tag1, tag2, tag3..."
            className="search"
          />
          <hr />
        </nav>
        <ImagesContainer>
          <div className="image-box">
            <img src="https://source.unsplash.com/featured/?disaster" alt="" />
          </div>
        </ImagesContainer>
      </>
    );
  }
}
