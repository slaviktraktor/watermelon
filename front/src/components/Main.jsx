import React from 'react';
import { Switch, Route } from 'react-router-dom';

import Feed from './Feed.jsx';
import New from '../containers/New.jsx';

import './Main.scss';

const Main = () => (
  <main>
    <Switch>
      <Route exact path="/new" component={New} />
      <Route path="/" component={Feed} />
    </Switch>
  </main>
);

export default Main;
