import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import EnzymeToJson from 'enzyme-to-json';
import { configure, shallow  } from 'enzyme';

import Feed from "./Feed";

configure({adapter: new Adapter()});

it("Feed snapshot", () => {
  const component = shallow(
    <Feed />
  );

  const tree = EnzymeToJson(component);
  expect(tree).toMatchSnapshot();
})