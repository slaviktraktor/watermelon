import React from 'react';
import { Link } from 'react-router-dom';
import './Header.scss';

const Header = () => (
  <header>
    <div className="wrapper">
      <div className="header-box">
        <Link className="logo" to="/">
          Watermelon
        </Link>
        <Link className="plus" to="/new">
          &nbsp;
        </Link>
      </div>
    </div>
  </header>
);

export default Header;
