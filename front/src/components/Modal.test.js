import React from "react";
import Adapter from 'enzyme-adapter-react-16';
import EnzymeToJson from 'enzyme-to-json';
import { mount, configure  } from 'enzyme';

import Modal from "./Modal";

configure({adapter: new Adapter()});
it("Open modal", () => {
  const component = mount(
    <Modal />
  );

  const tree = EnzymeToJson(component);
  expect(tree).toMatchSnapshot();
});

it("Find kek text in modal", () => {
  const text = "kek"
  const component = mount(
    <Modal>
      <div className="kek">{text}</div>
    </Modal>
  );

  expect(component.find("div.kek").text()).toEqual(text);
});