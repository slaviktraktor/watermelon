import React from 'react';
import ReactDOM from 'react-dom';

import './index.css';
import {Root} from './Root.jsx';
import * as serviceWorker from './serviceWorker';
import rootReducer from "./reducers/index";

//redux ******

import { createStore, applyMiddleware } from "redux";
//devtools + middleware *********
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";

const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(thunk)))

ReactDOM.render(<Root store={store} />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
