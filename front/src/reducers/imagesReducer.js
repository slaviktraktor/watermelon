const initialState = {
  list: [],
  current: null,
  meta: {
    name: '',
    tags: [],
  },
};

export default function images(state = initialState, action) {
  switch (action.type) {
    case 'ADD_IMAGE':
      return {
        ...state,
      };
    case 'ADD_IMAGE_ERROR':
      return {
        ...state,
      };
    case 'GET_START_IMAGES':
      return {
        ...state,
        list: action.payload,
      };
    case 'GET_ADDITIONAL_IMAGES':
      return {
        ...state,
        list: [...state.list, action.payload],
      };
    case 'GET_IMAGES_ERROR':
      return {
        ...state,
      };
    case 'OPEN_BIG_IMAGE':
      return {
        ...state,
        current: action.payload,
      };
    case 'IMAGE_META':
      return {
        ...state,
        meta: action.payload,
      };
    case 'CLOSE_BIG_IMAGE':
      return {
        ...state,
        current: undefined,
      };
    default:
      return state;
  }
}
