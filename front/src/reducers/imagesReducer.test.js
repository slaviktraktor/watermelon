import images from "./imagesReducer"; 

it("GET_ADDITIONAL_IMAGES test", () => {
  const payload = "kek"
  const state = images({list: []}, {type: "GET_ADDITIONAL_IMAGES", payload});

  expect(state.list[0]).toEqual(payload);
});

it("OPEN_BIG_IMAGE test", () => {
  const payload = "kek";
  const state = images({}, {type: "OPEN_BIG_IMAGE", payload});

  expect(state.current).toEqual(payload);
});

it("CLOSE_BIG_IMAGE test", () => {
  const state = images({}, {type: "OPEN_BIG_IMAGE"});

  expect(state.current).toEqual(undefined);
});