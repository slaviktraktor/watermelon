import React, { Component } from 'react';
import { closeBigAction, loadImageMeta } from '../../actions/imagesActions';
import { connect } from 'react-redux';
import Modal from '../../components/Modal.jsx';
import { withRouter } from 'react-router-dom';

import './BigImage.scss';

class BigImage extends Component {
  constructor(props) {
    super(props);

    this.props.loadMeta(this.props.match.params.img);
  }

  close = () => {
    this.props.closeBig();
    this.props.history.push('/');
  };

  render() {
    return (
      <Modal className="modal-back" onClose={this.close}>
        <div className="big-img">
          <div className="img-container">
            <img
              src={`${process.env.REACT_APP_SERVER_IP}images/img/${this.props.match.params.img}`}
              alt={this.props.match.params.img}
            />
          </div>
          <div className="description">
            <h1>{this.props.meta.title || 'Изображение'}</h1>
            <p className="desc">{this.props.meta.desc}</p>
            <p className="download">
              Скачать:{' '}
              <a
                href={`${process.env.REACT_APP_SERVER_IP}images/img/${this.props.match.params.img}`}
                rel="noopener noreferrer"
                target="_blank"
              >
                {this.props.meta.name}
              </a>
            </p>
            <p className="tags">
              {this.props.meta.tags.map((v, i) => {
                return <span key={i}>#{v}</span>;
              })}
            </p>
          </div>
        </div>
      </Modal>
    );
  }
}

function mapStateToProps(state) {
  return { currentImg: state.images.current, meta: state.images.meta };
}

function mapDispatchToProps(dispatch) {
  return {
    closeBig: () => dispatch(closeBigAction()),
    loadMeta: img => dispatch(loadImageMeta(img)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(BigImage));
