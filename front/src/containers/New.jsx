import React, { Component } from 'react';
import * as imgActions from '../actions/imagesActions';
import { connect } from 'react-redux';
import './New.scss';

class New extends Component {
  submit = e => {
    e.preventDefault();

    let reader = new FileReader();
    reader.readAsDataURL(this.refs.file.files[0]);
    let thisObj = this;
    reader.onloadend = function() {
      let URLfile = reader.result.replace(/data:[a-z/]+;base64,/, '');
      let name = thisObj.refs.file.files[0].name;
      let type = thisObj.refs.file.files[0].type;
      let tags = thisObj.refs.tags.value.trim().split(/\s*,\s*/);
      let title = thisObj.refs.title.value;
      let desc = thisObj.refs.desc.value;
      thisObj.props.addImageFunc(URLfile, name, type, title, desc, tags);
    };
  };

  getFileName = () => {
    let file = document.getElementById('uploaded-file').value;
    file = file
      .replace(/\\/g, '/')
      .split('/')
      .pop();
    document.getElementById('upfile-name').innerHTML = file;

    if (this.refs.file.files && this.refs.file.files[0]) {
      var reader = new FileReader();

      reader.onload = function(e) {
        document
          .getElementById('uploaded-img')
          .setAttribute('src', e.target.result);
      };

      reader.readAsDataURL(this.refs.file.files[0]);
    }
  };

  render() {
    return (
      <div className="image-form-container">
        <form className="new-image" onSubmit={this.submit}>
          <input
            type="text"
            ref="title"
            className="img-name"
            placeholder="Title"
            required
          />
          <input
            type="text"
            ref="tags"
            className="img-tags"
            placeholder="tag1, tag2, tag3..."
            required
          />
          <textarea ref="desc" placeholder="Description"></textarea>

          <label className="upload">
            <input
              type="file"
              ref="file"
              onChange={this.getFileName}
              id="uploaded-file"
              name="image"
              required
            />
            <span className="choose-file">Выберите файл</span>
            <span id="upfile-name"></span>
          </label>

          <label className="is-it-ok">
            <input type="checkbox" name="isfree" required />
            Подтверждаю, что данное изображение не находится под авторским
            правом и разрешаю использовать и распространять его бесплатно
          </label>
          <button type="submit">Send</button>
          <hr />
        </form>
        <div className="upload-img-container">
          <img src="" alt="upload_image" id="uploaded-img" />
        </div>
      </div>
    );
  }
  // <img src={this.img} alt="upload_image" ref="uploaded"/> <input type="file"
  // className="upload" ref="file" accept=".jpg, .jpeg, .png" required/>
}

function mapDispatchToProps(dispatch) {
  return {
    addImageFunc: (file, name, type, title, desc, tags) =>
      dispatch(imgActions.addImageAction(file, name, type, title, desc, tags)),
  };
}

export default connect(null, mapDispatchToProps)(New);

// 8-996-77-19-360 - Lexa
