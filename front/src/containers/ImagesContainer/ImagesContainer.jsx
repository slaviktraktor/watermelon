import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as imgActions from '../../actions/imagesActions';
import { images as configImg } from '../../etc/config.json';
import BigImage from '../BigImage/BigImage.jsx';

import './ImagesContainer.scss';

class ImagesContainer extends Component {
  constructor(props) {
    super(props);
    this.props.loadStartImages();
  }

  tryOpenBig(img) {
    this.props.openBig(img);
    this.props.history.push(`/${img}`);
  }

  render() {
    return (
      <h1>
        <div className="flexbin flexbin-margin">
          {this.props.imageList.map((v, i) => (
            <div
              className="image-box"
              key={i}
              onClick={() => this.tryOpenBig(v)}
            >
              <img src={`${process.env.REACT_APP_SERVER_IP}images/img/${v}`} alt={v} />
            </div>
          ))}
        </div>
        <Route path="/:img" component={BigImage} />
      </h1>
    );
  }
}

function mapStateToProps(state) {
  return { imageList: state.images.list, current: state.images.current };
}

function mapDispatchToProps(dispatch) {
  return {
    loadStartImages: () =>
      dispatch(imgActions.loadImagesAction(configImg.countPerPage, 1, true)),
    loadImages: (count, page, start = false) =>
      dispatch(imgActions.loadImagesAction(count, page, start)),
    openBig: img => dispatch(imgActions.openBigAction(img)),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(ImagesContainer));
