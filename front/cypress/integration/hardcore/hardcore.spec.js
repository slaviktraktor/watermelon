/// <reference types="cypress" />

const address = "http://127.0.0.1:1337/";

const getList = () => {
  setTimeout(() => {
    cy.request("POST", `${address}images/list/`);
  }, 0);
};

context("Hardcore", () => {
  it("List 100", () => {
    for (let i = 0; i < 100; i++) {
      cy.request("POST", `${address}images/list/`, { timeout: 100 });
    }
  });

  it("List 500", () => {
    for (let i = 0; i < 500; i++) {
      cy.request("POST", `${address}images/list/`, { timeout: 100 });
    }
  });

  it("List 1 000", () => {
    for (let i = 0; i < 1000; i++) {
      cy.request("POST", `${address}images/list/`, { timeout: 100 });
    }
  });

  it("List 2 000", () => {
    for (let i = 0; i < 2000; i++) {
      cy.request("POST", `${address}images/list/`, { timeout: 100 });
    }
  });

  it("Main page 100", () => {
    for (let i = 0; i < 100; i++) {
      cy.request("/", { timeout: 100 });
    }
  });

  it("Main page 500", () => {
    for (let i = 0; i < 500; i++) {
      cy.request("/", { timeout: 100 });
    }
  });

  it("Main page 1 000", () => {
    for (let i = 0; i < 1000; i++) {
      cy.request("/", { timeout: 100 });
    }
  });
});
