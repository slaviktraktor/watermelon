/// <reference types="cypress" />

context("Main", () => {
  beforeEach(() => {
    cy.visit("/new");
  });

  it("Add image to form", () => {
    cy.fixture("download.png")
      .as("download")
      .get("input[type=file]", { force: true })
      .then(function(el) {
        return Cypress.Blob.base64StringToBlob(this.download, "image/png").then(
          blob => {
            const name = "kek.png";
            const testFile = new File([blob], name, { type: "image/png" });
            const dataTransfer = new DataTransfer();
            dataTransfer.items.add(testFile);
            el[0].files = dataTransfer.files;
            el[0].dispatchEvent(new Event("change", { bubbles: true }));
          }
        );
      });

    cy.fixture("imgurl.json").then(data => {
      cy.get("#uploaded-img")
        .invoke("attr", "src")
        .should("equal", data.url);
    });
  });

  it("Click submit without entering data not sending request", () => {
    cy.get(".new-image button").click();

    cy.get(".img-name").should("be.focused");
  });

  it("Type name, tags and description", () => {
    cy.get(".img-name")
      .type("name")
      .should("have.value", "name");

    cy.get(".img-tags")
      .type("tag1, tag2, tag3")
      .should("have.value", "tag1, tag2, tag3");

    cy.get("textarea")
      .type("description")
      .should("have.value", "description");
  });

  it("Click submit after entering data", () => {
    cy.server();
    cy.route("POST", "images/add", "Object saved ez").as("addImage");

    cy.fixture("download.png")
      .as("download")
      .get("input[type=file]", { force: true })
      .then(function(el) {
        return Cypress.Blob.base64StringToBlob(this.download, "image/png").then(
          blob => {
            const name = "kek.png";
            const testFile = new File([blob], name, { type: "image/png" });
            const dataTransfer = new DataTransfer();
            dataTransfer.items.add(testFile);
            el[0].files = dataTransfer.files;
            el[0].dispatchEvent(new Event("change", { bubbles: true }));
          }
        );
      });

    cy.get(".img-name").type("name");
    cy.get(".img-tags").type("tag1, tag2");
    cy.get("textarea").type("description");
    cy.get("input[type='checkbox']").check();

    cy.get(".new-image button").click();

    cy.wait("@addImage").then(res => {
      expect(res.responseBody).equal("Object saved ez");
    });
  });
});
