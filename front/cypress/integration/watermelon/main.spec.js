/// <reference types="cypress" />

context("Main", () => {
  beforeEach(() => {
    cy.visit("/");
  });

  it("Open big img", () => {
    cy.get(".image-box:first").click();

    cy.get(".big-img").should("exist");
  });

  it("Close big img", () => {
    cy.get(".image-box:first").click();
    cy.get(".big-img").should("exist");
    cy.get(".modal-back").click("topLeft", { force: true });
    cy.get(".big-img").should("not.exist");
  });

  it("Put some tags into input field", () => {
    cy.get("input.search")
      .type("tag1, tag2, tag3")
      .should("have.value", "tag1, tag2, tag3")
      .clear();
  });

  it("Title should be watermelon", () => {
    cy.title().should("include", "Watermelon");
  });

  it("Every tag should starts with #", () => {
    cy.get(".image-box:first").click();

    cy.get(".big-img").should("exist");

    cy.get(".big-img .tags")
      .should("exist")
      .find("span")
      .invoke("text")
      .should("include", "#");
  });

  it("Download image", () => {
    cy.get(".image-box:first").click();

    cy.get(".big-img").should("exist");

    cy.get(".big-img .download")
      .invoke("text")
      .should("include", "Скачать:");

    cy.get(".big-img .download")
      .find("a")
      .should("exist")
      .invoke("text")
      .should("exist");
  });
});
